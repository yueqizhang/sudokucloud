package com.example.sudokucloud2;


import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

public class SocketThread extends Thread {

	private String portName;
	private int portNum;
	Socket soc = null;

	public SocketThread(String name, int num) {
		portName = name;
		portNum = num;
	}

	public void run() {
		try {
			soc = new Socket(portName, portNum);
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public Socket getSocket() {
		return soc;
	}
}
