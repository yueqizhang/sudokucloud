package com.example.sudokucloud2;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StreamCorruptedException;
import java.net.Socket;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;
import edu.gatech.util.Utility;



public class MainActivity extends ActionBarActivity {
	
	public static final int PORT_NUM = 9999;
	public static final String PORT_NAME = "192.168.1.110";
	// fir03 IP:128.61.241.233
	public long startTimeAll;
	public long endTimeAll;
	public long methodStart;
	public long methodEnd;

   // EditText ed;
    EditText tv;
    Button gen,copy;
    ProgressBar pb;
    int[][] bs = new int[9][9];
    Socket soc;
   // private StartAppAd startAppAd = new StartAppAd(this);
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
     //   StartAppSDK.init(this, "112014756", "202241970", true);
        setContentView(R.layout.activity_main);
        //ed = (EditText) findViewById(R.id.editText);
        tv = (EditText) findViewById(R.id.textView);
        gen = (Button) findViewById(R.id.button);
        copy = (Button) findViewById(R.id.copy);

        pb = (ProgressBar) findViewById(R.id.progressBar);
        pb.setVisibility(View.INVISIBLE);
        tv.setText("Click Generate");
        tv.setGravity(Gravity.CENTER_HORIZONTAL);
        //tv.setKeyListener(null);
        //final Tasker task = new Tasker();
        copy.setEnabled(false);
        
        SocketThread socketThread = new SocketThread(PORT_NAME, PORT_NUM);
		socketThread.start();
		try {
			socketThread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		soc = socketThread.getSocket();
        
        copy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                android.content.ClipboardManager clipboard = (android.content.ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                android.content.ClipData clip = android.content.ClipData.newPlainText("Clip",tv.getText().toString());
                Toast.makeText(getApplicationContext(),"Copied Selection",Toast.LENGTH_SHORT).show();
                clipboard.setPrimaryClip(clip);
            }
        });
        gen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startTimeAll = System.nanoTime();
               // task.execute(bs,bs,bs);
                Toast.makeText(getApplicationContext(),"Generating Combination...",Toast.LENGTH_SHORT).show();
                Toast.makeText(getApplicationContext(),"Please wait...",Toast.LENGTH_SHORT).show();
                 tv.setText("");
                new  Tasker().execute();

            }
        });
}
    class Tasker extends AsyncTask<Void, Void, Void> {

                @Override
        protected Void doInBackground(Void... params) {
            methodStart = System.nanoTime();
            try {
				bs =genSudokuClient();
				methodEnd = System.nanoTime();
			} catch (StreamCorruptedException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
                    return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (bs == null){
            	tv.append("Error! Generated null.");
            	pb.setVisibility(View.INVISIBLE);
                copy.setEnabled(true);
                return;
            }
            for (int i =0;i<9;i++){
                for (int j=0;j<9;j++){
                    if(j==3)
                        tv.append("| ");
                    if(j==6) tv.append("| ");

                    tv.append(String.valueOf(bs[i][j]));
                }
                tv.append("\n");
                if(i==2) tv.append("- - - - - - - - - - -\n");
                if(i==5) tv.append("- - - - - - - - - - -\n");
            }
            endTimeAll = System.nanoTime();
            tv.append(((endTimeAll - startTimeAll)/1000000.0) + " milliseconds" + "\n");
            tv.append(((methodEnd - methodStart)/1000000.0) + "ms (method)" + "\n");
            pb.setVisibility(View.INVISIBLE);
            copy.setEnabled(true);
        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            copy.setEnabled(false);
            pb.setVisibility(View.VISIBLE);
        }
    }
    
    public int[][] genSudokuClient() throws StreamCorruptedException, IOException{
    	int [][] genSudoku = new int [9][9];
    	if(soc == null){
				Log.v("genSudokuClient()", "soc is null!");
		}else{
			OutputStream toServer = new BufferedOutputStream(soc.getOutputStream());
			toServer.flush();
			InputStream fromServer = new BufferedInputStream(soc.getInputStream());
			Utility.simpleProtocolWrite(toServer, true, null);
			genSudoku = (int[][])Utility.simpleProtocolRead(fromServer, int[][].class, null);
		 }		
    	return genSudoku;
    }

    @Override
    public void onBackPressed() {

        Context context = this;
        final AlertDialog.Builder alert = new AlertDialog.Builder(context);
        alert.setTitle("About");
        alert.setMessage("Do you want to quit ?");
        alert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {
                dialog.cancel();
                MainActivity.this.finish();

            }
        });
        alert.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {
                dialog.cancel();
            }
        });
        AlertDialog aialog = alert.create();
        aialog.show();
       // super.onBackPressed();
    }
    @Override
    public void onResume() {
        super.onResume();
     
    }
    @Override
    public void onPause() {
        super.onPause();
    
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {


        return super.onOptionsItemSelected(item);
    }
}