package com.example.sudokucloud;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StreamCorruptedException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;
import java.util.concurrent.ExecutionException;

import utils.SudokuGen;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;
import edu.gatech.util.Utility;



public class MainActivity extends ActionBarActivity {
	
	public static final int PORT_NUM = 9999;
	public static final String PORT_NAME = "192.168.1.110";
	// fir03 IP:128.61.241.233
	static BufferedInputStream fromServer;
	static BufferedOutputStream toServer;
	public long startTimeAll;
	public long endTimeAll;
	public long methodStart;
	public long methodEnd;
	
   // EditText ed;
    EditText tv;
    Button gen,copy;
    ProgressBar pb;
    Socket soc;
   // private StartAppAd startAppAd = new StartAppAd(this);
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
     //   StartAppSDK.init(this, "112014756", "202241970", true);
        setContentView(R.layout.activity_main);
        //ed = (EditText) findViewById(R.id.editText);
        tv = (EditText) findViewById(R.id.textView);
        gen = (Button) findViewById(R.id.button);
        copy = (Button) findViewById(R.id.copy);

        pb = (ProgressBar) findViewById(R.id.progressBar);
        pb.setVisibility(View.INVISIBLE);
        tv.setText("Click Generate");
        tv.setGravity(Gravity.CENTER_HORIZONTAL);
        //tv.setKeyListener(null);
        //final Tasker task = new Tasker();
        copy.setEnabled(false);
        
        try {
			new CreateSocketTask().execute().get();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        copy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                android.content.ClipboardManager clipboard = (android.content.ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                android.content.ClipData clip = android.content.ClipData.newPlainText("Clip",tv.getText().toString());
                Toast.makeText(getApplicationContext(),"Copied Selection",Toast.LENGTH_SHORT).show();
                clipboard.setPrimaryClip(clip);
            }
        });
        gen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startTimeAll = System.nanoTime();
               // task.execute(bs,bs,bs);
                Toast.makeText(getApplicationContext(),"Generating Combination...",Toast.LENGTH_SHORT).show();
                Toast.makeText(getApplicationContext(),"Please wait...",Toast.LENGTH_SHORT).show();
                tv.setText("");
//                copy.setEnabled(false);
//                pb.setVisibility(View.VISIBLE);
//                generateSudoku();
//                displaySudoku();
				new  Tasker().execute();
				}
        });
}
    
    class Tasker extends AsyncTask<Void, Void, Void> {
    	int[][] sudoku;
        @Override
		protected Void doInBackground(Void... params) {
		    methodStart = System.nanoTime();
		    Generator gen = new Generator();
		    sudoku = gen.ComputeSudoku();
		    methodEnd = System.nanoTime(); //measures the time of the method
		            return null;
		}
		
		@Override
		protected void onPostExecute(Void aVoid) {
		    super.onPostExecute(aVoid);
		    for (int i =0;i<9;i++){
		        for (int j=0;j<9;j++){
		            if(j==3)
		                tv.append("| ");
		            if(j==6) tv.append("| ");
		
		            tv.append(String.valueOf(sudoku[i][j]));
		        }
		        tv.append("\n");
		        if(i==2) tv.append("- - - - - - - - - - -\n");
		        if(i==5) tv.append("- - - - - - - - - - -\n");
		    }
		    endTimeAll = System.nanoTime();
		    tv.append(((endTimeAll - startTimeAll)/1000000.0) + " ms (all)" + "\n");
		    tv.append(((methodEnd - methodStart)/1000000.0) + "ms (method)" + "\n");
		    pb.setVisibility(View.INVISIBLE);
		    copy.setEnabled(true);
		}
		@Override
		protected void onPreExecute() {
		    super.onPreExecute();
		    copy.setEnabled(false);
		    pb.setVisibility(View.VISIBLE);
		}
    }
    
    
    
    
//    public void displaySudoku(){
//    	try{
//    		sud.join();
//    	}catch(InterruptedException e){
//    		e.printStackTrace();
//    	}
//
//    	 if (sud.sudoku == null){
//         	tv.append("Error! Generated null.");
//         	pb.setVisibility(View.INVISIBLE);
//             copy.setEnabled(true);
//             return;
//         }
//         for (int i =0;i<9;i++){
//             for (int j=0;j<9;j++){
//                 if(j==3)
//                     tv.append("| ");
//                 if(j==6) tv.append("| ");
//
//                 tv.append(String.valueOf(sud.sudoku[i][j]));
//             }
//             tv.append("\n");
//             if(i==2) tv.append("- - - - - - - - - - -\n");
//             if(i==5) tv.append("- - - - - - - - - - -\n");
//         }
//         endTimeAll = System.nanoTime();
//         tv.append(((endTimeAll - startTimeAll)/1000000.0) + " milliseconds" + "\n");
//         tv.append(((methodEnd - methodStart)/1000000.0) + "ms (method)" + "\n");
//         pb.setVisibility(View.INVISIBLE);
//         copy.setEnabled(true);
//    }
    
//    public void generateSudoku(){
//    	methodStart = System.nanoTime();
//    	sud = new GeneratorThread();
//    	sud.start();
//		methodEnd = System.nanoTime();
//    }
    
    private class CreateSocketTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            try {
				soc = new Socket(PORT_NAME, PORT_NUM);
				soc.setTcpNoDelay(true);
			} catch (UnknownHostException e1) {
				e1.printStackTrace();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
         Log.v("CreateSocketTask", "Socket created");
            if(soc != null){
	            try {
				toServer = new BufferedOutputStream(soc.getOutputStream());
				toServer.flush();
		        fromServer = new BufferedInputStream(soc.getInputStream());
				} catch (IOException e) {
					e.printStackTrace();
				}
	        }
			return null;
        }
    }
 

    @Override
    public void onBackPressed() {

        Context context = this;
        final AlertDialog.Builder alert = new AlertDialog.Builder(context);
        alert.setTitle("About");
        alert.setMessage("Do you want to quit ?");
        alert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {
                dialog.cancel();
                MainActivity.this.finish();

            }
        });
        alert.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {
                dialog.cancel();
            }
        });
        AlertDialog aialog = alert.create();
        aialog.show();
       // super.onBackPressed();
    }
    @Override
    public void onResume() {
        super.onResume();
     
    }
    @Override
    public void onPause() {
        super.onPause();
    
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        return super.onOptionsItemSelected(item);
    }
    
}