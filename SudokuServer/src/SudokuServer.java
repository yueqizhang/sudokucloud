import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;

import edu.gatech.util.Utility;

public class SudokuServer {
	public static final int PORT_NUM = 9999;
	public static Boolean check = true;

	public static ArrayList<Integer> arFromClient;
	public static void main(String[] args) throws IOException {
		ServerSocket serverSocket = new ServerSocket(PORT_NUM);
		System.out.println("Server was successful");
		Socket clientSocket = serverSocket.accept();
		clientSocket.setTcpNoDelay(true);
		OutputStream toClient = new BufferedOutputStream(
				clientSocket.getOutputStream());
		toClient.flush();
		InputStream fromClient = new BufferedInputStream(
				clientSocket.getInputStream());
		int[][] sudoku = new int [9][9];
		arFromClient = (ArrayList<Integer>)Utility.simpleProtocolRead(fromClient, ArrayList.class, null);
		System.out.println("Row received: " + arFromClient.toString());
		for(int j=0;j<9;j++){
			sudoku[0][j] = arFromClient.get(j);
		}
		int times = 0;
        for(int i=0;i<9;i++){
        	times++;
			System.out.println("Row received; " + i);
			
            if(i==3){
                check = checkPath(sudoku, i);
                if(check==false){
                	i=i-2;
                }
            }else if(i==6){
                check = checkPath(sudoku,i);
                if(check==false){
                    i=i-2;
                }
            }
            if(i>0) {
                check = checker(sudoku, i);
                if(check==false){
                    i--;
                }
            }
            Utility.simpleProtocolWrite(toClient, check, null);
            Utility.simpleProtocolWrite(toClient, i, null);
			System.out.println("Writing to Client: " + check);
			arFromClient = (ArrayList<Integer>)Utility.simpleProtocolRead(fromClient, ArrayList.class, null);
        }
        System.out.println("Finished, " + times + " times");
        serverSocket.close();
	}	           	        
	
	
    public static boolean checker(int[][] bs, int i) {
        ArrayList<Integer> temp_ar = new ArrayList<Integer>();
        boolean check1 = true;          //For returning true for good Sudoku
        for(int e=0;e<9;e++){
            bs[i][e] = arFromClient.get(e);
        }
        for(int t=0;t<9;t++){
            for(int e=0;e<=i;e++){
                temp_ar.add(e, bs[e][t]);
            }
            Set<Integer> temp_set = new HashSet<Integer>(temp_ar);
            if(temp_set.size()<temp_ar.size()){
                check1 = false; break;                                    
            }
            else {
                temp_ar.clear();
                temp_set.clear();
            }
        }
        return check1;
    }

    public static boolean checkPath(int[][] bs, int i) {
        //boolean check_cP = false;
        ArrayList<Integer> temp_cP = new ArrayList<Integer>();
        Set<Integer> temp_setcP = new HashSet<Integer>();
        boolean denoter = true;
        while(i==3){
            for(int k =0;k<i;k++ ){
                for(int e=0;e<3;e++){
                    temp_cP.add(e, bs[k][e]);
                }
            }
            temp_setcP = new HashSet<Integer>(temp_cP);
            if(temp_cP.size()>temp_setcP.size()){
                denoter = false;break;

            }
            else {
                temp_cP.clear();
                temp_setcP.clear();

                for(int k =0;k<i;k++ ){
                    for(int e=3;e<6;e++){
                        temp_cP.add(bs[k][e]);
                    }
                }
                temp_setcP = new HashSet<Integer>(temp_cP);
                if(temp_cP.size()>temp_setcP.size()){
                    denoter = false;break;

                }
                else {
                    break;
                }
            }
        }
        while(i==6){

            for(int k =3;k<i;k++ ){
                for(int e=0;e<3;e++){
                    temp_cP.add(e, bs[k][e]);
                }
            }
            temp_setcP = new HashSet<Integer>(temp_cP);
            if(temp_cP.size()>temp_setcP.size()){
                denoter = false;break;

            }
            else {
                temp_cP.clear();
                temp_setcP.clear();

                for(int k =3;k<i;k++ ){
                    for(int e=3;e<6;e++){
                        temp_cP.add(bs[k][e]);
                    }
                }
                temp_setcP = new HashSet<Integer>(temp_cP);
                if(temp_cP.size()>temp_setcP.size()){
                    denoter = false;break;

                }
                else {
                    break;
                }
            }

        }
        return denoter;
    }

}


